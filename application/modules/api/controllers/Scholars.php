<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scholars extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	/*  */
	public function index_get()
	{
		// $data = json_decode(file_get_contents('php://input'));

		$response = [];

		$this->db->join('students', 'students.student_id = scholars.student_id');
		$this->db->join('sponsors', 'sponsors.id = scholars.sponsor');
		$this->db->join('assessment_group', 'assessment_group.student_id = students.student_id');

		if ($this->get('sponsor_page')) {
			$this->db->select(['students.student_id','students.last_name','students.first_name','students.middle_name','students.course_code', 'assessment_group.balance']);
		}
		else {
			$this->db->select(['students.student_id','students.last_name','students.first_name','students.middle_name','students.course_code', 'sponsors.name', 'assessment_group.balance']);
		}

		if ($this->get('q'))
			$this->db->where('YEAR(assessment_group.datetime)=', $this->get('q'));
		if ($this->get('batch'))
			$this->db->where('YEAR(assessment_group.datetime)=', $this->get('batch'));
		if ($this->get('sponsor'))
			$this->db->where('sponsor', $this->get('sponsor'));

		$response['data'] = $this->db->get('scholars');

		$this->table->set_template([ 'table_open'=>'<table class="table table-striped table-bordered table-hover">', 'heading_row_start'=>'<tr class="active">' ]);

		if ($this->get('sponsor_page')) {
			$this->table->set_heading(['Student ID', 'Last Name', 'First Name', 'Middle Name', 'Course', 'Balance']);
		}
		else {
			$this->table->set_heading(['Student ID', 'Last Name', 'First Name', 'Middle Name', 'Course', 'Sponsor', 'Balance']);
		}

		$response['html'] = $this->table->generate($response['data']);

		if (!$response['data']->num_rows()) $response['html'] = '<p class="text-center alert alert-warning">No result found</p>';

		$response['data'] = $response['data']->result();

		$this->response($response);
	}
}